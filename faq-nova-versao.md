---
layout: page
title: FAQ
permalink: /faq-wip/
wip: true
---

<details><summary>O que são a carteirinha da USP e o BUSP?</summary>

A carteirinha da USP é necessária retirar livros da biblioteca e ter acesso a alguns lugares do campus. Existem duas linhas de ônibus (8012-10 e 8022-10) que percorrem o campus e vão até o metrô Butantã. Com o BUSP você pode usar o circular de graça.

</details>
<details><summary>Como solicitar a carteirinha USP e o BUSP?</summary>

![](busp.jpg)

</details>

<details><summary>Onde fica o bandejão?</summary>

Há quatro restaurantes universitários no campus (apelidados de bandejão):

1. Restaurante Central (próximo à praça do relógio - CRUSP)
2. Restaurante da Física (no IF-USP)
3. Restaurante da Química (no IQ-USP)
4. Restaurante da Prefeitura (no PUSP-C)

O bandejão mais próximo do IME é o restaurante da física.
</details>


<details><summary>Como solicitar/renovar o bilhete único?</summary>

Os alunos de pós-graduação têm direito a um desconto de 50% para o uso de transporte público pela SPTrans ou EMTU (EMTU apenas para quem não mora em São Paulo) através do passe escolar. O passe escolar da SPTrans (bilhete único escolar) permite o uso de ônibus em São Paulo e o uso de trens e metrô na região metropolitana.

Ao contrário do que acontece para alunos de graduação, a cota do bilhete único (quantidade de passagens que um aluno consegue comprar com desconto durante um mês) é calculada de acordo com a quantidade de dias da semana em que você vai assistir aula no semestre em que você solicitou o bilhete único. 

Para solicitar o bilhete único você precisa comprovar a quantidade de dias que você frequenta a USP. Existem duas formas de fazer isso.

1. Você pode imprimir o comprovante de matrícula no Janus com a lista de disciplinas.
2. Outra possibilidade é preencher o formulário de declaração de frequência, indicando quais dias da semana você vai para a USP estudar. Você precisa pedir o formulário na secretaria. Nesse caso você precisa da assinatura do orientador.  Você também precisa imprimir o comprovante de matrícula simples no Janus.

Nos dois casos é necessário entregar o documento no SAS

Após a USP realizar o cadastro, você precisa se cadastrar no site da SPTrans e pagar o boleto. No início de cada ano é necessário repetir esse processo para renovar o bilhete.

</details>

<details><summary>O IME possui cursos de verão?</summary>

Sim, o instituto oferece cursos de verão. Muitos deles são voltados para alunos de graduação e para o público geral. Esses listados neste link http://ime.usp.br/~verao Existem também cursos de verão voltados para alunos de pós. Veja as disciplinas aqui: https://www.ime.usp.br/pos/ferias Você pode aproveitar os créditos dos cursos, mas apenas dos cursos voltados para a pós **conferir**

Você não precisa ter começado a pós para fazer os cursos de verão da pós. Confira o [site](https://www.ime.usp.br/pos/ferias) para mais informações

</details>

<details><summary>Quantas disciplinas preciso cursar?</summary>

Não existe uma quantidade fixa de disciplinas que você precisa cursar. No entanto, para se formar é necessário conseguir uma quantidade de créditos, obtidos quase totalmente pela defesa bem sucedida da dissertação ou tese e pela aprovação em disciplinas de pós. As disciplinas de pós no IME normalmente oferecem 8 créditos.

A quantidade de créditos é diferente para alunos de mestrado e doutorado:

**Perguntar**

Mestrado: no mestrado você precisa de X créditos
Você precisa de X créditos para se formar, sendo que X créditos são obtidos ao concluir a dissertação. Você concluir os X créditos restantes cursando 8 discilpinas de pós no IME.

Doutorado: no doutorado você precisa de X créditos
Você precisa de X créditos para se formar, sendo que X créditos são obtidos ao concluir a dissertação. Você concluir os X créditos restantes cursando 8 discilpinas de pós no IME.

Doutorado direto: no doutorado direto você precisa de X créditos
Você precisa de X créditos para se formar, sendo que X créditos são obtidos ao concluir a dissertação. Você concluir os X créditos restantes cursando 8 discilpinas de pós no IME.

Além das disciplinas, você pode conseguir créditos de outras formas. Os cursos de verão da pós também oferecem créditos. Durante o semestre você pode fazer minicursos, que durante quase sempre menos de uma semana e oferecem 2 créditos. Além disso, a partição em eventos científicos **(apenas com apresentação de trabalho/poster???)** também contam créditos. No entanto, os créditos de minicursos e eventos científicos não podem somar mais que 6**(???)** créditos durante a pós.

Não existe um limite máximo **conferir** de disciplinas que você pode cursar durante o semestre, mas os alunos do IME cursam, por semestre, X disciplinas (na pura e aplicada), X na computação... É importante não se sobrecarregar, pois uma reprovação ou nota baixa pode causar o cancelamento da bolsa ou prejudicar sua vida acadêmica futura.

</details>

<details><summary>Como me inscrevo nas disciplinas?</summary>

1. Acesse o [Janus](https://uspdigital.usp.br/janus)
2. ...
** Conferir: A primeira matrícula é feita na secretaria, e as outras no Janus. Falar que o orientador e o professor da disciplina deve autorizar pelo site e que é bom (quase obrigatório) a opinião do orientador antes de se inscrever.

</details>

<details><summary>Qual é a data limite para o exame de qualificação e o exame de proficiência em línguas?</summary>

**Explicar que alunos estrangeiros também precisam fazer a prova de português**

Você pode verificar essas informações no [Janus](https://uspdigital.usp.br/janus) na opção Aluno Regular > Ficha do Aluno.

</details>

<details><summary>Posso morar no CRUSP?</summary>

Sim, porém você precisa passar por um processo de seleção socioeconômico. Para mais informações, acesse a opção Apoio Estudantil > Pós-Graduação > Moradia Pós-Graduação em http://sites.usp.br/sas/ 

Se você não for de São Paulo e não possuir bolsa, você pode conseguir vaga para alojamento emergencial durante um tempo provisório. Mais informações em Apoio Estudantil > Pós-Graduação > Alojamento Emergencial em http://sites.usp.br/sas/ 

**Explicar que as vagas são concorridas. Indicar grupos de aluguel de quartos no facebook. O preço médio é de X reais**
</details>

<details><summary>Quais são as disciplinas oferecidas neste semestre?</summary>

Ainda não divulgaram a lista de disciplinas para o próximo semestre. As disciplinas do semestre atual estão nos links abaixo:

Bioinformática: https://www.ime.usp.br/bioinfo/pos/oferecidas
Computação: https://www.ime.usp.br/dcc/pos/disciplinas/horarios
Matemática: https://www.ime.usp.br/mat/pos/disciplinas/horarios
Matemática Aplicada: https://www.ime.usp.br/map/pos/disciplinas-oferecidas
MPEM: https://www.ime.usp.br/posmpemat/disciplinas-oferecidas
</details>

<details><summary>Como não perder a bolsa?</summary>

**Preciso de ajuda aqui!**
- Não reprovar em duas disciplinas
- matrícula de acompanhamento
- exame de idiomas e qualificação

</details>

<details><summary>Quem é a pessoa no logo do IME?</summary>

O rosto que está no logo do IME é do matemático grego Arquimedes. Leia mais [aqui](https://www.ime.usp.br/images/arquivos/aconteceime/acontecenoime_edicao_33.pdf).

</details>
