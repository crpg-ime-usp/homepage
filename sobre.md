---
layout: page
title: Sobre
permalink: /sobre/
---

### O que é a Recepção de Pós-Graduação?

A Recepção de Pós-Graduação é um evento tradicional onde é dada as boas-vindas para todas pessoas que ingressam na pós-graduação do IME, independentemente do programa. Ela acontece sempre na tarde da primeira terça-feira de cada semestre da pós.

O objetivo da Recepção é integrar e informar as pessoas que ingressam na pós. Para isso, temos uma quantidade de atividades, que inclui visitas nos prédios do IME, apresentações dos coletivos e grupos que existem no IME e também uma apresentação das regras da pós bem com os espaços e atividades disponíveis do IME, da USP, e de cada programa. Temos momentos para tirar dúvidas com seus veteranos e coordenadores do programa. É um evento o ingressante conhecer seu instituto da visão de estudantes de pós e também seus colegas e as pessoas com quais você vai conviver nos próximos anos.

Acreditamos que é crucial que as pessoas se sintam bem no seu ambiente de trabalho e estudo e que para isso, é necessário se sentir pertencente ao espaço e conhecer as pessoas que frequentam o mesmo lugar. É muito mais confortável se movimentar dentro de um espaço onde conhecemos as regras e sabemos o que é esperado.

Aprender, estudar e pesquisar fica mais fácil quando conseguimos compartilhar com outras pessoas, afinal a pesquisar é uma atividade coletiva e não individual. Há muitas iniciativas dentro do IME que valem à pena conhecer, e com a Recepção tentamos mostrar algumas delas para vocês. E como um dia não é o suficiente para mostrar esse universo novo, organizamos outras atividades de integração ao longo dos semestres, para explorar o IME, a USP e São Paulo.
o.

### Quem somos nós?

Somos uma comissão composta por estudantes da pós que se voluntariam para trazer a Recepção aos estudantes ingressantes na pós em cada semestre. Tentamos sempre ter uma pessoa por programa na comissão, para não esquecer de levar em consideração as realidades diferentes que temos dentro do nosso instituto. Nossas reuniões acontecem uma ou duas vezes por mês, recentemente sendo a sala de reuniões do bloco C o nosso local de preferência.

Sempre aceitamos novas pessoas para ajudar na Recepção, participando na organização durante o semestre, ou então no dia do evento, onde sempre precisamos de mais pessoas.

Adoramos quando pessoas nos abordam com novas ideias e iniciativas. Acreditamos que cada ponto de vista agrega para que consigamos construir um evento melhor. Muito do que há na Recepção atualmente existe por conta de uma sugestão individual, feita no passado e forjada por discussão e apoio.

### História da Recepção

A Recepção organizada por estudantes surgiu em uma tentativa de unir as iniciativas organizadas por alguns coordenadores de programa para receber novas turmas de estudantes. Queríamos que houvesse uma recepção para todos os programas, que acontecesse todo semestre e que fosse em um horário que possibilitasse a participação de todas e todos.

![Foto da Recepção de 2013]({{ "/imgs/recepcao2013.jpg" | relative_url }})

A primeira Recepção de Pós-Graduação unificada ocorreu no primeiro semestre de 2013. A partir desse ponto, e até o segundo semestre de 2016, o formato da Recepção era relativamente enxuto, o evento acontecia durante o horário de almoço e, dessa forma, tinha menos variedade nas atividades. Havia, como há hoje, a distribuição de canecas e o tradicional lanche com sanduíches de metro, naquela época absolutamente vital por conta do horário.

A partir do primeiro semestre de 2017, a comissão foi reforçada pelo ingresso de um novo grupo de estudantes, um novo conjunto de ideias, e alguma ousadia como acompanhamento. Assim, a Recepção cresceu consideravelmente, o evento passou a decorrer por uma tarde, após o almoço, de tal forma que possibilitou que a Recepção comportasse, entre outras, breves introduções por parte das muitas iniciativas que existem no IME e apresentações artísticas.

![Foto da recepção do semestre 2 de 2018]({{ "/imgs/recepcao2018-2.JPG" | relative_url }})