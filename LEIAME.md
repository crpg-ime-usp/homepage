
# Página da Comissão de Recepção de Pós-Graduação do IME-USP

[Conteúdo licensiado sob Creative Commons SA-BY 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)

URL oficial: https://ime.usp.br/~rpg

## Como mexer na página

Em teoria, só editar os arquivos diretamente no site do GitLab. O site
oficial é atualizado a cada hora.

(mentira, isso ainda não tá feito, mas parece fácil)

## Como configurar um ambiente local

1. [Instale o `rvm`](https://rvm.io/rvm/install)
2. Use sempre `bash --login`
3. `rvm install ruby-2.4.0`
4. Entre na pasta do repositório
5. `rvm current` tem que dizer `ruby-2.4.0@crpg-homepage`
6. `bundle install`

Se quiser gerar a página localmente, use `jekyll build`. Ele vai colocar na
pasta `_site`.

O resto é editar as páginas e commitar no git.

