---
layout: page
title:  "Recepção 2022"
permalink: /apresentacoes2022-1/
---

Slides para download e links da recepção de **2022** :
- [Apresentação de Boas vindas]({{ "/docs/2022-1/2022-Apresentação.pdf" | absolute_url }})
- [Tour pelo IME - fotos]({{ "/docs/2021-1/Tour-IME.pdf" | absolute_url}})
- [Sobre a Biblioteca]({{ "/docs/2020-2/folder-biblioteca.pdf" | absolute_url }})
- [Apresentação da Biblioteca]({{ "/docs/2022-1/Biblioteca_apresentação_2022-1.pdf" | absolute_url }})
- [Utilização das salas de estudo]({{ "/docs/2022-1/uso-de-salas.pdf" | absolute_url }})
- [Wiki da Rede do IME](https://wiki.ime.usp.br/)

Apresentações das CCPs:
- [CCP - MAT]({{ "/docs/2021-1/CCP-MAT-2021.pdf" | absolute_url }})
- [CCP - MAP]({{ "/docs/2022-1/CCP-MAP_2022-1.pdf" | absolute_url }})
- [CCP - MAC]({{ "/docs/2022-1/CCP-MAC_2022-1.pdf" | absolute_url }})
- [CCP - MAE]({{ "/docs/2022-1/CCP-MAE_2022-1.pdf" | absolute_url }})
- [CCP - MPEM]({{ "/docs/2022-1/CCP-MPEM_2022-1.pdf" | absolute_url }})
- [CPG - Bioinfo]({{ "/docs/2022-1/CPG-Bioinfo-2022.pdf" | absolute_url }})

Aos que quiserem ter acesso aos materiais de recepções anteriores, entre em contato por e-mail.
Faça parte da Comissão nos próximos semestres.
