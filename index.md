---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
list_title: "Notícias"
---
![Foto da recepção]({{ "/imgs/recepcao2018-1.jpeg" | relative_url }})


# Recepção de Pós-Graduação


Olá! Essa página é dedicada à Recepção da Pós-Graduação do IME-USP.

Aqui divulgamos as informações relacionadas a Recepção de Pós Graduação (RPG), assim como alguns materiais que distribuímos no dia da recepção.

A recepção para ingressantes do primeiro semestre de 2022 ocorrerá presencialmente no dia 22 de Março de 2022 a partir das 13h no Instituto de Matemática e Estatística - USP, Bloco B sala B05.
Será obrigatório o uso de máscaras e apresentação do comprovante de vacinação (duas doses ou única).

Se você for ingressante, entre em contato com rpg@ime.usp.br para entrar no grupo do Whats e receber mais informações.



