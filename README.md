
# Página da Comissão de Recepção de Pós-Graduação do IME-USP

Esse repositório contém os arquivos-fonte da [página oficial](https://ime.usp.br/~rpg) da Comissão de Recepção da Pós-Graduação do IME-USP.

Todo o conteúdo aqui está [licenciado sob a Creative Commons SA-BY 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR).

## Como mexer na página

A página funciona usando duas tecnologias: git e [jekyll](https://jekyllrb.com/). Para poder editar o conteúdo da página, não é preciso entender como elas funcionam mas, caso haja interesse, explicamos um pouco mais pro final do texto. Por enquanto, o importante é saber que o git é responsável por sincronizar as versões da página e o jekyll é responsável por compilar o código-fonte aqui para o site que você vê quando acessa o endereço oficial em www.ime.usp.br/~rpg.

### Passo 1: fazer uma conta em [gitlab.com](https://gitlab.com)

GitLab é o site onde hospedamos os arquivos-fonte. Você precisa fazer uma conta para poder submeter alterações à página.

### Passo 2: editar o conteúdo pela Web IDE do GitLab

Siga para a [página de capa do repositório](https://gitlab.com/crpg-ime-usp/homepage) e clique em "Web IDE" (o botão não aparece se você não tiver feito login ainda). À sua esquerda, você terá um mini-navegador mostrando as pastas e arquivos do repositório. Cada arquivo `*.md` ou `*.markdown` equivale a uma seção da página da comissão. Em particular, a edição mais comum na página é fazer um post novo, que é o que vamos usar de exemplo.

Ache a pasta `_posts` e expanda ela. Ela já deve ter vários arquivos de posts. Para fazer um novo post, você precisa criar um arquivo nesta pasta com um nome no formato `202X-MM-DD-exemplo-de-post.markdown`. A data é a data que o post será publicado.

Dentro do arquivo, o texto deve também seguir um formato estipulado:

```markdown
---
layout: post
title:  "Exemplo de Post"
date:   2020-02-04 14:00:00 -0300
categories: jekyll update
---

Olás! Esse é meu post de exemplo!

Nesta parte do arquivo (depois dos "---") você pode escrever normalmente, mas tem como fazer uma formatação mais elaborada, se preferir. Para isso, veja a especificação da linguagem markdown. Abaixo tem um link para uma cola simples:

[Exemplo de link](https://www.markdownguide.org/cheat-sheet/)
```

A parte no começo do arquivo é o cabeçalho e precisa ter todos os campos do exemplo:

+ `layout`: indica o tipo de página – no caso, um `post`
+ `title`: título do post, entre aspas; é o que aparece no site
+ `date`: data de publicação, o post não aparece antes desse dia e horário!!!
+ `categories`: campo de uso interno do jekyll, deixe como no exemplo

### Passo 3: submetendo suas mudanças

Uma vez que você tenha criado e alterado todos os arquivos-fonte que quiser, clique no botão "Commit" na parte esquerda inferior da Web IDE do GitLab. Ele irá mostrar na parte esquerda todos os arquivos modificados. Certifique-se de que todos estejam abaixo de "Staged" e clique em "Commit" novamente.

Deve aparecer um formulário de submissão de "Merge Request", que é um pedido de alteração na página. Se quiser, você pode detalhar suas mudanças para facilitar o processo de revisão. Quando estiver tudo pronto, clique em "Submit merge request".

Agora, os admins do repositório serão notificados da sua mudança e poderão revisá-la antes de incluí-la oficialmente na página.

Caso *você* faça parte dos admins, você pode usar a opção "Commit to master" e pular a etapa de revisão.

### Passo 4: colocando as mudança no ar

Mesmo depois de aprovadas, suas mudanças só vão aparecer na página oficial no dia seguinte. Isso ocorre porque a página é compilada. Existe um script que automatiza o processo e compila ela uma vez por dia. Se precisar muito, entre em contato com kazuo at ime.usp.br e diga que se trata de uma atualização urgente.

## Sobre o Git

Git é uma ferramenta de versionamento voltada para edição colaborativa de código-fonte. Ela permite que as versões de um mesmo código em diversas máquinas seja mantido em sincronia. Para isso, ela cria "linhas do tempo paralelas" chamadas *branches* e pode uní-las (fazer *merge*) quando necessário. Além de possibilitar o desenvolvimento distribuído, o git acrescente um nível de segurança pois os desenvolvedores podem controlar quais *branches* estão adequadas o suficiente para entrar na "linha do tempo principal" (normalmente a *branch* "master").

O GitLab, por sua vez, é um serviço software livre de hospedagem de repositórios versionados por git. Escolhemos ele devido à sua funcionalidade "Web IDE", que permite as pessoas editarem um repositório sem precisar usar git manualmente.

## Sobre o Jekyll

Jekyll é uma ferramenta para compilar páginas Web. Ele converte arquivos escritos com Markdown para HTML, garantindo uma relação um-para-um entre arquivos markdown e páginas de um site. É o que chamamos de um gerador estático de HTML porque, cada vez que os arquivos-fonte são alterados, é preciso manualmente compilá-los e colocá-los no ar. É um processo mais burocrático que uma ferramenta de HTML mais dinâmica (como wordpress) mas, em compensação, não exige um servidor Web e todo o trabalho (e custos) de manutenção que viriam com um.

## Avançado: como configurar um ambiente local

Se quiser, e souber como usar git manualmente, você pode editar a página localmente no seu computador:

1. [Instale o `rvm`](https://rvm.io/rvm/install)
2. Use sempre `bash --login`
3. `rvm install ruby-2.4.0`
4. Entre na pasta do repositório
5. `rvm current` tem que dizer `ruby-2.4.0@crpg-homepage`
6. `bundle install`

Se quiser gerar a página localmente, use `jekyll build`. Ele vai colocar na
pasta `_site`.

O resto é editar as páginas e commitar no git.
