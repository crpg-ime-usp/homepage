---
layout: page
title: Material
permalink: /material/

# Em construção. Em breve teremos o manual de sobrevivência aqui.
---

Veja nosso manual de sobrevivência [aqui]({{ "/docs/2019-2/Manual_de_Sobrevivencia.pdf" | absolute_url }})

*Find our survival guide [here]({{ "/docs/2019-2/Survival_Guide.pdf" | absolute_url }})*

Regimentos dos Programas de Pós Graduação:
- [MAT]({{ "/docs/regulamentos/MAT-NORMAS-2021s1.pdf" | absolute_url }})
- [MAC]({{ "/docs/regulamentos/MAC-NORMAS-2021s1.pdf" | absolute_url }})
- [MAP]({{ "/docs/regulamentos/MAP-Regimento-2020_novo.pdf" | absolute_url }})
- [MAE]({{ "/docs/regulamentos/MAE-NORMAS-2021s1.pdf" | absolute_url }})
- [MPEM]({{ "/docs/regulamentos/MPEM-NORMAS-2021s1.pdf" | absolute_url }})
- [Bioinformática]({{ "/docs/regulamentos/BIOINFO-NORMAS-2021s1.pdf" | absolute_url }})
- [Regimento Pós Graduação USP]({{ "/docs/regulamentos/regimento_pos_usp.pdf" | absolute_url }})


As informações que estão no manual estão em constante mudança.
Nós nos esforçamos para manter o manual atualizado e completo, mas não garantimos completamemte ambos.
Caso você ache uma informação incorreta por favor nos avise. Alternativamente, abra uma *issue* ou
submeta um *merge request* [direto no repositório do manual]({{ "https://gitlab.com/crpg-ime-usp/manual-de-sobrevivencia" | absolute_url }}).

Se tiver alguma dúvida, entre em contato por e-mail (rpg@ime.usp.br) ou pelo facebook
[conosco]({{ "https://www.facebook.com/groups/117482945522023/?ref=bookmarks" | absolute_url }})

Aqui está o [itinerário dos circulares]({{ "/docs/2018-2/itinerarios-circulares.pdf" | absolute_url }})
