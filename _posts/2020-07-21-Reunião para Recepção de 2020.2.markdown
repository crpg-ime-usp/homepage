---
layout: post
title:  "Reunião para Recepção de 2020.2"
date:   2020-07-20 19:25:00 -0300
categories: jekyll update
---

Olás!

A Comissão de Recepção de Pós-Graduação está preparando a recepção do segundo semestre de 2020 e nós gostaríamos de convidar a todxs que queiram contribuir para a organização do evento!

Faremos nossa próxima reunião esta terça-feira, 21/07, das 13h30min às 15h30min, virtualmente em meet.google.com/gry-fevi-ecw. Venha conhecer a comissão e nosso trabalho =)

No momento estamos testando alguns joguinhos para integração!
