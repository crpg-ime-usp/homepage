---
layout: post
title:  "Recepção do segundo semestre de 2018"
date:   2018-08-08 15:17:41 -0300
categories: jekyll update
---

Ontem foi a recepção de pós-graduação do segundo semesre de 2018, contando pela
primeira vez com apresentação de dança moderna e *tour* pelo IME.

[Aqui estão os slides que usamos]({{ "/docs/2018-2/Apresentação.pdf" | absolute_url }})

[And here is the English version of the slides]({{ "/docs/2018-2/Presentation.pdf" | absolute_url }})

[Aqui tem o manual de sobrevivência completo (English included)]({{ "/docs/2018-2/manual-pos.pdf" | absolute_url }})

Apresentações das CCPs (nem todas tiveram slides):

+ [CCP-MAT]({{ "/docs/2018-2/ccp-mat.pdf" | absolute_url }})
+ [CCP-MAP]({{ "/docs/2018-2/ccp-map.pdf" | absolute_url }})
+ [CCP-MAE]({{ "/docs/2018-2/ccp-mae.pdf" | absolute_url }})


Alguns dos slides das entidades que se apresentaram:

+ [CinIME]({{ "/docs/2018-2/cinime.pdf" | absolute_url }})
+ [Biblioteca]({{ "/docs/2018-2/biblioteca.pdf" | absolute_url }})

Por último, aqui tem o [itinerário dos circulares]({{ "/docs/2018-2/itinerarios-circulares.pdf" | absolute_url }})

